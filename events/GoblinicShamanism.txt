
namespace = goblinic_shamanism_flavor

#Unlocking cults from other religions by owned provinces
country_event = {
	id = goblinic_shamanism_flavor.0
	title = goblinic_shamanism_flavor.0.t #Never Seen
	desc = goblinic_shamanism_flavor.0.t #Never Seen
	picture = POPE_PREACHING_eventPicture #Never Seen
	
	hidden = yes
		
	immediate = {
		random_owned_province = {
			limit = {
				goblinic_shamanism_trigger = yes
			}
			goblinic_shamanism_event = yes
		}
	}

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		any_owned_province = {
			goblinic_shamanism_trigger = yes
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.0.t #Never seen
	}
}	

#Unlocking cults from other religions by alliance
country_event = {
	id = goblinic_shamanism_flavor.1
	title = goblinic_shamanism_flavor.1.t #Never Seen
	desc = goblinic_shamanism_flavor.1.t #Never Seen
	picture = POPE_PREACHING_eventPicture #Never Seen
	
	hidden = yes
		
	immediate = {
		random_neighbor_country = {
			limit = {
				alliance_with = ROOT
				goblinic_shamanism_trigger = yes
			}
			goblinic_shamanism_event = yes
		}
	}

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		any_neighbor_country = {
			alliance_with = ROOT
			goblinic_shamanism_trigger = yes
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.1.t #Never seen
	}
}

#Generic cults
#Unlocking Kroozmuta Zemna
country_event = {
	id = goblinic_shamanism_flavor.20
	title = goblinic_shamanism_flavor.20.t
	desc = goblinic_shamanism_flavor.20.d
	picture = TRIBES_MEETING_eventPicture

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		NOT = { has_unlocked_cult = kroozmuta_zemna_cult }
		NOT = { primary_culture = cave_goblin }
		any_neighbor_country = {
			religion = goblinic_shamanism
			has_adopted_cult = kroozmuta_zemna_cult
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.20.a
		unlock_cult = kroozmuta_zemna_cult
	}
}

#Unlocking Kroozbuto Nubo
country_event = {
	id = goblinic_shamanism_flavor.21
	title = goblinic_shamanism_flavor.21.t
	desc = goblinic_shamanism_flavor.21.d
	picture = TRIBES_MEETING_eventPicture

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		NOT = { has_unlocked_cult = kroozbuto_nubo_cult }
		NOT = { primary_culture = common_goblin }
		any_neighbor_country = {
			religion = goblinic_shamanism
			has_adopted_cult = kroozbuto_nubo_cult
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.21.a
		unlock_cult = kroozbuto_nubo_cult
	}
}

#Unlocking Tewaka Sunka
country_event = {
	id = goblinic_shamanism_flavor.22
	title = goblinic_shamanism_flavor.22.t
	desc = goblinic_shamanism_flavor.22.d
	picture = TRIBES_MEETING_eventPicture

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		NOT = { has_unlocked_cult = tewaka_sunka_cult }
		NOT = { primary_culture = exodus_goblin }
		any_neighbor_country = {
			religion = goblinic_shamanism
			has_adopted_cult = tewaka_sunka_cult
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.22.a
		unlock_cult = tewaka_sunka_cult
	}
}

#Unlocking Muta Mestika - UNUSED
# country_event = {
# 	id = goblinic_shamanism_flavor.23
# 	title = goblinic_shamanism_flavor.23.t
# 	desc = goblinic_shamanism_flavor.23.d
# 	picture = TRIBES_MEETING_eventPicture

# 	trigger = {
# 		religion = goblinic_shamanism
# 		has_dlc = "Rights of Man"
# 		NOT = { has_unlocked_cult = muta_mestika_cult }
# 		NOT = { primary_culture = hill_goblin }
# 		any_neighbor_country = {
# 			religion = goblinic_shamanism
# 			has_adopted_cult = muta_mestika_cult
# 		}
# 	}

# 	mean_time_to_happen = {
# 		months = 36
# 	}

# 	option = {
# 		name = goblinic_shamanism_flavor.23.a
# 		unlock_cult = muta_mestika_cult
# 	}
# }

#Unlocking Apzarko Hrom
country_event = {
	id = goblinic_shamanism_flavor.24
	title = goblinic_shamanism_flavor.24.t
	desc = goblinic_shamanism_flavor.24.d
	picture = TRIBES_MEETING_eventPicture

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		NOT = { has_unlocked_cult = apzarko_hrom_cult }
		NOT = { primary_culture = common_goblin }
		any_neighbor_country = {
			religion = goblinic_shamanism
			has_adopted_cult = apzarko_hrom_cult
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.24.a
		unlock_cult = apzarko_hrom_cult
	}
}

#Unlocking Vortza Lesa
country_event = {
	id = goblinic_shamanism_flavor.25
	title = goblinic_shamanism_flavor.25.t
	desc = goblinic_shamanism_flavor.25.d
	picture = TRIBES_MEETING_eventPicture

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		NOT = { has_unlocked_cult = vortza_lesa_cult }
		NOT = { primary_culture = forest_goblin }
		any_neighbor_country = {
			religion = goblinic_shamanism
			has_adopted_cult = vortza_lesa_cult
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.25.a
		unlock_cult = vortza_lesa_cult
	}
}

#Unlocking Vortzo Laukuno
country_event = {
	id = goblinic_shamanism_flavor.26
	title = goblinic_shamanism_flavor.26.t
	desc = goblinic_shamanism_flavor.26.d
	picture = TRIBES_MEETING_eventPicture

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		NOT = { has_unlocked_cult = vortzo_laukuno_cult }
		NOT = { primary_culture = exodus_goblin }
		any_neighbor_country = {
			religion = goblinic_shamanism
			has_adopted_cult = vortzo_laukuno_cult
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.26.a
		unlock_cult = vortzo_laukuno_cult
	}
}

#Unlocking Vortza Krpa
country_event = {
	id = goblinic_shamanism_flavor.27
	title = goblinic_shamanism_flavor.27.t
	desc = goblinic_shamanism_flavor.27.d
	picture = TRIBES_MEETING_eventPicture

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		NOT = { has_unlocked_cult = vortza_krpa_cult }
		NOT = { primary_culture = cave_goblin }
		any_neighbor_country = {
			religion = goblinic_shamanism
			has_adopted_cult = vortza_krpa_cult
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.27.a
		unlock_cult = vortza_krpa_cult
	}
}

#Unlocking Zvaiko Udenzo
country_event = {
	id = goblinic_shamanism_flavor.28
	title = goblinic_shamanism_flavor.28.t
	desc = goblinic_shamanism_flavor.28.d
	picture = TRIBES_MEETING_eventPicture

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		NOT = { has_unlocked_cult = zvaiko_udenzo_cult }
		NOT = { primary_culture = exodus_goblin }
		any_neighbor_country = {
			religion = goblinic_shamanism
			has_adopted_cult = zvaiko_udenzo_cult
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.28.a
		unlock_cult = zvaiko_udenzo_cult
	}
}

#Unlocking Zvaika Kirba
country_event = {
	id = goblinic_shamanism_flavor.29
	title = goblinic_shamanism_flavor.29.t
	desc = goblinic_shamanism_flavor.29.d
	picture = TRIBES_MEETING_eventPicture

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		NOT = { has_unlocked_cult = zvaika_kirba_cult }
		NOT = { primary_culture = common_goblin }
		any_neighbor_country = {
			religion = goblinic_shamanism
			has_adopted_cult = zvaika_kirba_cult
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.29.a
		unlock_cult = zvaika_kirba_cult
	}
}

#Unlocking Vortzo Liezmo
country_event = {
	id = goblinic_shamanism_flavor.30
	title = goblinic_shamanism_flavor.30.t
	desc = goblinic_shamanism_flavor.30.d
	picture = TRIBES_MEETING_eventPicture

	trigger = {
		religion = goblinic_shamanism
		has_dlc = "Rights of Man"
		NOT = { has_unlocked_cult = vortzo_liezmo_cult }
		NOT = { primary_culture = cave_goblin }
		any_neighbor_country = {
			religion = goblinic_shamanism
			has_adopted_cult = vortzo_liezmo_cult
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	option = {
		name = goblinic_shamanism_flavor.30.a
		unlock_cult = vortzo_liezmo_cult
	}
}

# general events

# Seeking council from the ancestors
country_event = {
	id = goblinic_shamanism_flavor.40
	title = "goblinic_shamanism_flavor.40.t"
	desc = "goblinic_shamanism_flavor.40.d"
	picture = BATTLE_eventPicture

	trigger = {
		religion = goblinic_shamanism
	}

	mean_time_to_happen = {
		months = 1200
	}

	option = {
		name = "goblinic_shamanism_flavor.40.a"
		increase_legitimacy_medium_effect = yes
		ai_chance = {
			factor = 1
			modifier = { 
				NOT = { legitimacy = 40 }
				factor = 2
			}
			modifier = { 
				NOT = { legitimacy = 60 }
				factor = 2
			}
		}
	}

	option = {
		name = "goblinic_shamanism_flavor.40.b"
		add_mil_power = 50
		ai_chance = {
			factor = 1
			modifier = {
				personality = ai_militarist
				factor = 3
			}
		}
	}

	option = {
		name = "goblinic_shamanism_flavor.40.c"
		add_dip_power = 50
		ai_chance = {
			factor = 1
			modifier = {
				personality = ai_diplomat
				factor = 3
			}
		}
	}

	option = {
		name = "goblinic_shamanism_flavor.40.e"
		add_adm_power = 50
		ai_chance = {
			factor = 1
			modifier = {
				personality = ai_capitalist
				factor = 3
			}
		}
	}

	option = {
		name = "goblinic_shamanism_flavor.40.f"
		add_prestige = 5
	}
}

# deity specific events

# Young daughter wants to go to war
country_event = {
	id = goblinic_shamanism_flavor.50
	title = "goblinic_shamanism_flavor.50.t"
	desc = "goblinic_shamanism_flavor.50.d"
	picture = BATTLE_eventPicture

	trigger = {
		has_adopted_cult = muta_mestika_cult
		is_at_war = yes
		is_heir_leader = yes
	}

	mean_time_to_happen = {
		months = 120
	}

	option = {
		name = "goblinic_shamanism_flavor.50.a"
		define_general = {
			fire = 3
			shock = 1
			manuever = 2
			siege = 1
			female = yes
		}
		add_prestige = 10
	}

	option = {
		name = "goblinic_shamanism_flavor.50.b"
		add_dip_power = 20
		add_prestige = -5
	}
}

# Discuss laukuno with lesaist ruler, triggering Vortzo Laukuno, the husband of Vortza Lesa
country_event = {
	id = goblinic_shamanism_flavor.51
	title = "goblinic_shamanism_flavor.51.t"
	desc = "goblinic_shamanism_flavor.51.d"
	picture = DIPLOMACY_eventPicture

	trigger = {
		has_adopted_cult = vortzo_laukuno_cult
		any_known_country = {
			marriage_with = ROOT
			has_adopted_cult = vortza_lesa_cult
			has_regency = no
			NOT = { has_ruler_flag = had_married_deities_event }
		}
	}

	mean_time_to_happen = {
		months = 120
	}

	option = {
		name = "goblinic_shamanism_flavor.51.a"
		add_dip_power = 20
		random_known_country = {
			limit = {
				marriage_with = ROOT
				has_adopted_cult = vortza_lesa_cult
				has_regency = no
				NOT = { has_ruler_flag = had_married_deities_event }
			}
			add_opinion = {
				who = ROOT
				modifier = reconciling_differences
			}
			hidden_effect = {
				country_event = { id = goblinic_shamanism_flavor.52 days = 1 }
			}
		}
	}
}

# Vortzo Laukuno, the husband of Vortza Lesa
country_event = {
	id = goblinic_shamanism_flavor.52
	title = "goblinic_shamanism_flavor.52.t"
	desc = "goblinic_shamanism_flavor.52.d"
	picture = CULTURE_eventPicture

	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			set_ruler_flag = had_married_deities_event
		}
	}

	# Contemplate Vortzo Laukuno as a medium to Vortza Lesa
	option = {
		name = "goblinic_shamanism_flavor.52.a"
		add_ruler_modifier = {
			name = hindu_withdrawn_contemplation
			duration = 720
		}
		hidden_effect = { country_event = { id = goblinic_shamanism_flavor.53 days = 720 } }
	}

	# Contemplate Vortzo Laukuno as peer to Vortza Lesa
	option = {
		name = "goblinic_shamanism_flavor.52.b"
		add_ruler_modifier = {
			name = hindu_withdrawn_contemplation
			duration = 720
		}
		hidden_effect = { country_event = { id = goblinic_shamanism_flavor.54 days = 720 } }
	}

	# Continue search
	option = {
		name = "goblinic_shamanism_flavor.52.c"
		add_prestige = 2
	}
}

# 
country_event = {
	id = goblinic_shamanism_flavor.53
	title = "goblinic_shamanism_flavor.53.t"
	desc = "goblinic_shamanism_flavor.53.d"
	picture = GOOD_WITH_MONARCH_eventPicture

	is_triggered_only = yes

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = "goblinic_shamanism_flavor.53.a"
		increase_ruler_dip_effect = yes
	}
}

# 
country_event = {
	id = goblinic_shamanism_flavor.54
	title = "goblinic_shamanism_flavor.54.t"
	desc = "goblinic_shamanism_flavor.54.d"
	picture = CULTURE_eventPicture

	is_triggered_only = yes

	mean_time_to_happen = {
		days = 1
	}

	# 
	option = {
		name = "goblinic_shamanism_flavor.54.a"
		add_ruler_modifier = { name = fertility_cult }
	}

	# I'll folow the path to prosperity
	option = {
		name = "goblinic_shamanism_flavor.54.b"
		increase_ruler_adm_effect = yes
		add_years_of_income = 0.05
		change_cult = vortzo_laukuno_cult
	}

	# Agriculture is population, population is military might
	option = {
		name = "goblinic_shamanism_flavor.54.c"
		increase_ruler_mil_effect = yes
		add_yearly_manpower = 0.1
		change_cult = vortzo_laukuno_cult
	}
}

#Rain prayer
country_event = {
	id = goblinic_shamanism_flavor.55
	title = goblinic_shamanism_flavor.55.t
	picture = FAMINE_eventPicture

	desc = {
		trigger = { has_adopted_cult = vortzo_laukuno_cult }
		desc = goblinic_shamanism_flavor.55.d.a
	}
	desc = {
		trigger = { NOT = { has_adopted_cult = vortzo_laukuno_cult } }
		desc = goblinic_shamanism_flavor.55.d.b
	}

	trigger = {
		has_dlc = "Rights of Man"
		NOT = { is_month = 4 }
		any_owned_province = {
			NOT = { has_province_modifier = rains_of_fertility }
			NOT = { has_province_modifier = barren_lands }
		}
		OR = {
			has_adopted_cult = vortzo_laukuno_cult
			has_adopted_cult = kroozbuto_nubo_cult
		}
	}

	mean_time_to_happen = {
		months = 500
	}
	
	immediate = {
		hidden_effect = {
			random_owned_province = {
				limit = { 
					trade_goods = grain 
					NOT = { has_province_modifier = rains_of_fertility }
					NOT = { has_province_modifier = barren_lands }
				}
				save_event_target_as = rain_province
			}

			if = {
				limit = { NOT = { has_saved_event_target = rain_province } }
				random_owned_province = {
					limit = {
						NOT = { has_province_modifier = rains_of_fertility }
						NOT = { has_province_modifier = barren_lands }
					}
					save_event_target_as = rain_province
				}
			}
		}
	}

	#Pray and sacrifice
	option = {
		name = goblinic_shamanism_flavor.55.a

		add_adm_power = -10
		add_years_of_income = 0.05

		set_country_flag = rain_prayers

		hidden_effect = {
			random_list = {
				67 = { country_event = { id = goblinic_shamanism_flavor.56 days = 90 } }
				33 = { country_event = { id = goblinic_shamanism_flavor.57 days = 90 } }
			}
		}
	}
	
	#Do nothing
	option = {
		name = goblinic_shamanism_flavor.55.b

		hidden_effect = {
			random_list = {
				33 = { country_event = { id = goblinic_shamanism_flavor.56 days = 90 } }
				67 = { country_event = { id = goblinic_shamanism_flavor.57 days = 90 } }
			}
		}
	}
}

#Rains of fertility
country_event = {
	id = goblinic_shamanism_flavor.56
	title = goblinic_shamanism_flavor.56.t
	picture = FARMING_eventPicture

	desc = {
		trigger = { has_country_flag = rain_prayers has_adopted_cult = vortzo_laukuno_cult }
		desc = goblinic_shamanism_flavor.56.d.a
	}
	desc = {
		trigger = { NOT = { has_country_flag = rain_prayers } has_adopted_cult = vortzo_laukuno_cult }
		desc = goblinic_shamanism_flavor.56.d.b
	}
	desc = {
		trigger = { has_country_flag = rain_prayers has_adopted_cult = kroozbuto_nubo_cult }
		desc = goblinic_shamanism_flavor.56.d.c
	}
	desc = {
		trigger = { NOT = { has_country_flag = rain_prayers } has_adopted_cult = kroozbuto_nubo_cult }
		desc = goblinic_shamanism_flavor.56.d.d
	}

	is_triggered_only = yes

	option = {
		name = goblinic_shamanism_flavor.56.a

		clr_country_flag = rain_prayers

		event_target:rain_province = {
			area = {
				limit = { owned_by = ROOT }
				add_province_modifier = {
					name = rains_of_fertility
					duration = 1825
				}
			}
		}
	}
}

#Drought
country_event = {
	id = goblinic_shamanism_flavor.57
	title = goblinic_shamanism_flavor.57.t
	picture = FAMINE_eventPicture

	desc = {
		trigger = { has_country_flag = rain_prayers has_adopted_cult = vortzo_laukuno_cult }
		desc = goblinic_shamanism_flavor.57.d.a
	}
	desc = {
		trigger = { NOT = { has_country_flag = rain_prayers } has_adopted_cult = vortzo_laukuno_cult }
		desc = goblinic_shamanism_flavor.57.d.b
	}
	desc = {
		trigger = { has_country_flag = rain_prayers has_adopted_cult = kroozbuto_nubo_cult }
		desc = goblinic_shamanism_flavor.57.d.c
	}
	desc = {
		trigger = { NOT = { has_country_flag = rain_prayers } has_adopted_cult = kroozbuto_nubo_cult }
		desc = goblinic_shamanism_flavor.57.d.d
	}

	is_triggered_only = yes

	option = {
		name = goblinic_shamanism_flavor.57.a

		clr_country_flag = rain_prayers
		
		event_target:rain_province = {
			area = {
				limit = { owned_by = ROOT }
				add_province_modifier = {
					name = barren_lands
					duration = 1825
				}
			}
		}
	}
}

# The temple destroyers
country_event = {
	id = goblinic_shamanism_flavor.58
	title = "goblinic_shamanism_flavor.58.t"
	desc = "goblinic_shamanism_flavor.58.d"
	picture = MILITARY_CAMP_eventPicture

	trigger = {
		has_adopted_cult = apzarko_hrom_cult
		has_regency = no
		NOT = { has_ruler_modifier = temple_destroyers }
		NOT = { has_ruler_flag = refused_temple_destroyers }
	}

	mean_time_to_happen = {
		months = 360
	}

	option = {
		name = "goblinic_shamanism_flavor.58.a"
		add_ruler_modifier = {
			name = temple_destroyers
			duration = 7200
		}
		add_yearly_manpower = 0.5
		every_neighbor_country  = {
			limit = {
				NOT = { religion = goblinic_shamanism }
			}
			add_opinion = {
				who = ROOT
				modifier = destruction_of_temples
			}
		}
	}

	option = {
		name = "goblinic_shamanism_flavor.58.b"
		add_dip_power = 20
		hidden_effect = {
			set_ruler_flag = refused_temple_destroyers
		}
	}		
}

# Convert temples
country_event = {
	id = goblinic_shamanism_flavor.59
	title = "goblinic_shamanism_flavor.59.t"
	desc = "goblinic_shamanism_flavor.59.d"
	picture = RELIGIOUS_TURMOIL_eventPicture

	trigger = {
		has_adopted_cult = apzarko_hrom_cult
		has_regency = no
		NOT = { has_ruler_modifier = conversion_of_temples }
		NOT = { has_ruler_flag = refused_conversion_of_temples }
	}

	mean_time_to_happen = {
		months = 360
	}

	option = {
		name = "goblinic_shamanism_flavor.59.a"
		add_ruler_modifier = {
			name = conversion_of_temples
			duration = 7200
		}
		every_neighbor_country  = {
			limit = {
				NOT = { religion = goblinic_shamanism }
			}
			add_opinion = {
				who = ROOT
				modifier = destruction_of_temples
			}
		}
	}

	option = {
		name = "goblinic_shamanism_flavor.59.b"
		add_adm_power = 20
		hidden_effect = {
			set_ruler_flag = refused_conversion_of_temples
		}
	}		
}

# foreign cult events

# 
country_event = {
	id = goblinic_shamanism_flavor.100
	title = "goblinic_shamanism_flavor.100.t"
	desc = "goblinic_shamanism_flavor.100.d"
	picture = FETISHIST_FIRE_eventPicture

	trigger = {
		has_adopted_cult = ancestor_worship_cult
	}

	mean_time_to_happen = {
		months = 1200
	}

	option = {
		name = "goblinic_shamanism_flavor.100.a"
		increase_legitimacy_small_effect = yes
		add_prestige = 5
	}
}

## Unlocking Cults

#Unlocking Cannorian
country_event = {
	id = goblinic_shamanism_flavor.2
	title = goblinic_shamanism_flavor.2.t
	desc = goblinic_shamanism_flavor.2.d
	picture = POPE_PREACHING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.2.a
		unlock_cult = cannorian_cult 
	}
}

#Unlocking Gnomish
country_event = {
	id = goblinic_shamanism_flavor.3
	title = goblinic_shamanism_flavor.3.t
	desc = goblinic_shamanism_flavor.3.d
	picture = BIG_BOOK_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.3.a
		unlock_cult = gnomish_cult 
	}
}

#Unlocking dwarven cult
country_event = {
	id = goblinic_shamanism_flavor.4
	title = goblinic_shamanism_flavor.4.t
	desc = goblinic_shamanism_flavor.4.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.4.a
		unlock_cult = ancestor_worship_cult 
	}
}

#Unlocking godlost cult
country_event = {
	id = goblinic_shamanism_flavor.401
	title = goblinic_shamanism_flavor.401.t
	desc = goblinic_shamanism_flavor.401.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.401.a
		unlock_cult = godlost_cult 
	}
}

#Unlocking kheteratan cult
country_event = {
	id = goblinic_shamanism_flavor.402
	title = goblinic_shamanism_flavor.402.t
	desc = goblinic_shamanism_flavor.402.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.402.a
		unlock_cult = kheteratan_cult 
	}
}

#Unlocking gnollish cult
country_event = {
	id = goblinic_shamanism_flavor.403
	title = goblinic_shamanism_flavor.403.t
	desc = goblinic_shamanism_flavor.403.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.403.a
		unlock_cult = gnollish_cult 
	}
}

#Unlocking elven cult
country_event = {
	id = goblinic_shamanism_flavor.404
	title = goblinic_shamanism_flavor.404.t
	desc = goblinic_shamanism_flavor.404.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.404.a
		unlock_cult = elven_cult 
	}
}

#Unlocking bulwari cult
country_event = {
	id = goblinic_shamanism_flavor.405
	title = goblinic_shamanism_flavor.405.t
	desc = goblinic_shamanism_flavor.405.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.405.a
		unlock_cult = bulwari_cult 
	}
}

#Unlocking gerudian cult
country_event = {
	id = goblinic_shamanism_flavor.406
	title = goblinic_shamanism_flavor.406.t
	desc = goblinic_shamanism_flavor.406.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.406.a
		unlock_cult = gerudian_cult 
	}
}

#Unlocking orcish cult
country_event = {
	id = goblinic_shamanism_flavor.407
	title = goblinic_shamanism_flavor.407.t
	desc = goblinic_shamanism_flavor.407.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.407.a
		unlock_cult = orcish_cult 
	}
}

#Unlocking harpy cult
country_event = {
	id = goblinic_shamanism_flavor.408
	title = goblinic_shamanism_flavor.408.t
	desc = goblinic_shamanism_flavor.408.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.408.a
		unlock_cult = harpy_cult 
	}
}

#Unlocking fey cult
country_event = {
	id = goblinic_shamanism_flavor.409
	title = goblinic_shamanism_flavor.409.t
	desc = goblinic_shamanism_flavor.409.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.409.a
		unlock_cult = fey_cult 
	}
}

#Unlocking raheni cult
country_event = {
	id = goblinic_shamanism_flavor.410
	title = goblinic_shamanism_flavor.410.t
	desc = goblinic_shamanism_flavor.410.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.410.a
		unlock_cult = raheni_cult 
	}
}

#Unlocking giantkin cult
country_event = {
	id = goblinic_shamanism_flavor.411
	title = goblinic_shamanism_flavor.411.t
	desc = goblinic_shamanism_flavor.411.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.411.a
		unlock_cult = giantkin_cult 
	}
}

#Unlocking centaur cult
country_event = {
	id = goblinic_shamanism_flavor.412
	title = goblinic_shamanism_flavor.412.t
	desc = goblinic_shamanism_flavor.412.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.412.a
		unlock_cult = centaur_cult 
	}
}


#Unlocking centaur cult
country_event = {
	id = goblinic_shamanism_flavor.413
	title = goblinic_shamanism_flavor.413.t
	desc = goblinic_shamanism_flavor.413.d
	picture = PRAYING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_dlc = "Rights of Man"
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = goblinic_shamanism_flavor.413.a
		unlock_cult = kobold_cult 
	}
}